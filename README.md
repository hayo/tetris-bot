# Tetris BOT



## But de ce dépot

Projet TIPE : réaliser un robot invincible à TETRIS

## WhitePaper :

## Roadmap :
-> Implémenter une version de Tetris en python <br>
-> Trouver des critères d'évaluation des plateaux <br>
    - attribuer un score à chaque configuration de plateau <br>
    - score : potentiel de libération d'une ligne<br>
-> Demander au robot d'évaluer toutes les configurations possibles avec la nouvelle pièce <br>
et choisir de placer la pièce dans la configuration avec le plus gros score.
(-> Possibilité d'évaluer 2 coup à l'avance car on connait la prochain pièce)<br>
<br>
-> Lancer une IA tetris et comparer les résultats.<br>
Lancer le jeu sur un site web et le site permet aux joueur d'affronter le bot.
Montrer les coups sur lequels le robot a été meilleur.<br>