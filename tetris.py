from settings import *
import math
from tetromino import Tetromino
import pygame.freetype as ft

class Text:
    def __init__(self, app):
        self.app = app
        self.font = ft.Font('Baristo.ttf')
    
    def draw(self):
        self.font.render_to(self.app.screen, (WIN_W * 0.595, WIN_H * 0.02),
                            text='TETRIS', fgcolor='white', 
                            size=TILE_SIZE * 1.65, bgcolor= (50, 50, 139))
        self.font.render_to(self.app.screen, (WIN_W * 0.65, WIN_H * 0.2),
                            text='SCORE :', fgcolor='white',
                            size=TILE_SIZE * 1.35, bgcolor=(50, 50, 139))
    
    def draw_score(self, score):
        self.font.render_to(self.app.screen, (WIN_W * 0.75, WIN_H * 0.275),
                            text=str(score), fgcolor='white',
                            size=TILE_SIZE * 1.2, bgcolor=(50, 50, 139))
        

class Tetris:
    def __init__(self, app):
        self.app = app
        self.sprite_group = pg.sprite.Group()
        self.field_array = self.get_field_array()
        self.tetromino = Tetromino(self)
        self.next_tetromino = Tetromino(self, current=False)
        self.speed_up = False
        self.score = 0
        self.level = 0
        self.lines = 0
        
    def check_full_lines(self):
        numbers = 0
        row = FIELD_H - 1
        for y in range(FIELD_H -1, -1, -1):
            for x in range(FIELD_W):
                self.field_array[row][x] = self.field_array[y][x] 
                
                if self.field_array[y][x]:
                    self.field_array[row][x].pos = vec(x, y)
            if sum(map(bool, self.field_array[y])) < FIELD_W:
                row -= 1 
            else:    
                numbers += 1
                for x in range(FIELD_W):
                    self.field_array[row][x].alive = False
                    self.field_array[row][x] = 0
        # points : A * (niv + 1)
        if numbers == 1:
            self.score += 40*(self.level + 1)
            self.lines += 1
        elif numbers == 2:
            self.lines += 2
            self.score += 100
        elif numbers == 3:
            self.lines += 3
            self.score += 300
        elif numbers == 1:
            self.lines += 4
            self.score += 1200
                    
    def put_tetromino_blocks_in_array(self):
        for block in self.tetromino.blocks:
            x, y = int(block.pos.x), int(block.pos.y)
            self.field_array[y][x] = block 
        
    def get_field_array(self):
        return [[0 for x in range(FIELD_W)] for y in range(FIELD_H)]
    
    def is_game_over(self):
        if self.tetromino.blocks[0].pos.y == INIT_POS_OFFSET[1]:
            pg.time.wait(300)
            return True
        
    def check_tetromino_landing(self):
        if self.tetromino.landing:
            if self.is_game_over():
                self.__init__(self.app)
            else:    
                self.speed_up = False
                self.put_tetromino_blocks_in_array()
                self.next_tetromino.current = True
                self.tetromino = self.next_tetromino
                self.next_tetromino = Tetromino(self, current=False)
        
    def draw_grid(self):
        for x in range(FIELD_W):
            for y in range(FIELD_H):
                pg.draw.rect(self.app.screen, (10,10,10), 
                            (x*TILE_SIZE,y*TILE_SIZE, TILE_SIZE, TILE_SIZE), 1)
    
    def control(self, pressed_key):
        if pressed_key == pg.K_LEFT:
            self.tetromino.move(direction='left')
        elif pressed_key == pg.K_RIGHT:
            self.tetromino.move(direction='right')
        elif pressed_key == pg.K_UP:
            self.tetromino.rotate()    
        elif pressed_key == pg.K_DOWN:
            # self.tetromino.move(direction='down')
            self.speed_up = True
        
    def update(self):
        print(self.level)
        trigger = [self.app.anim_trigger, self.app.fast_anim_trigger][self.speed_up]
        self.check_full_lines()
        if trigger:
            self.check_full_lines()
            self.tetromino.update()
            self.check_tetromino_landing()
        self.sprite_group.update()
        self.level = int(self.lines/10)
        
    def draw(self):
        self.draw_grid()
        self.sprite_group.draw(self.app.screen)